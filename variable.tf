variable "region" {
  default = "us-east-1"

}

variable "vpccidr" {
  default = "10.20.0.0/16"

}

variable "pub-sub-names" {
  default = ["ninja-pub-sub-01", "ninja-pub-sub-02"]

}

variable "pri-sub-names" {
  default = ["ninja-pri-sub-01", "ninja-pri-sub-02"]

}



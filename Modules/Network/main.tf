#creating VPC
resource "aws_vpc" "ninja_vpc" {

    cidr_block = var.vpccidr
    enable_dns_hostnames = true
    enable_dns_support = true

    tags = {
      "Name" = "ninja-vpc-01"
    }
  
}

#Creating internet Gateway
resource "aws_internet_gateway" "ninja_igw" {

    vpc_id = aws_vpc.ninja_vpc.id
    tags = {
      "Name" = "ninja_igw"
    }
  
}

#creating public subnet.
resource "aws_subnet" "public_subnet" {

    count = length(var.pub-sub-names)
    vpc_id = aws_vpc.ninja_vpc.id
    availability_zone = data.aws_availability_zones.ninja.names[count.index]
    cidr_block = cidrsubnet(var.vpccidr,8,count.index)

    tags = {
      "Name" = var.pub-sub-names[count.index]
    }

  
}

#creating private subnet.
resource "aws_subnet" "private_subnet" {

    count = length(var.pri-sub-names)
    vpc_id = aws_vpc.ninja_vpc.id
    availability_zone = data.aws_availability_zones.ninja.names[count.index]
    cidr_block = cidrsubnet(var.vpccidr,8,count.index + length(var.pub-sub-names))

    tags = {
      "Name" = var.pri-sub-names[count.index]
    }

  
}

#Creating EIP
resource "aws_eip" "ninja_eip" {
    vpc = true
  
}

#Creating NAT Gateway.
resource "aws_nat_gateway" "ninja_nat_gateway" {
    allocation_id = aws_eip.ninja_eip.id
    subnet_id = aws_subnet.public_subnet[0].id
    tags = {
      "Name" = "ninja-nat-01"
    }
  
}

#Creating Public Route Table.
resource "aws_route_table" "ninja_public_route" {
    vpc_id = aws_vpc.ninja_vpc.id
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.ninja_igw.id
    }

    tags = {
      "Name" = "ninja-route-pub"
    }
  
}


#Creating Private Route Table.
resource "aws_route_table" "ninja_private_route" {
    vpc_id = aws_vpc.ninja_vpc.id
    tags = {
      "Name" = "ninja-route-priv"
    }
  
}

#Associating Public Route table to Public subnets.
resource "aws_route_table_association" "public_association" {

    count = length(var.pub-sub-names)
    subnet_id = element(aws_subnet.public_subnet.*.id,count.index)
    route_table_id = aws_route_table.ninja_public_route.id
  
}

#Associating Private Route table to Private subnets.
resource "aws_route_table_association" "private_association" {

    count = length(var.pri-sub-names)
    subnet_id = element(aws_subnet.private_subnet.*.id,count.index)
    route_table_id = aws_route_table.ninja_private_route.id
  
}


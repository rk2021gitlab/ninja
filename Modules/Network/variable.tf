#cidr block of VPC
variable "vpccidr" {
    default = "10.0.0.0/16"
  
}

#Public Subnets Name
variable "pub-sub-names" {
    default = ["ninja-pub-sub-01","ninja-pub-sub-02"]
  
}

#Private Subnets Name
variable "pri-sub-names" {
    default = ["ninja-pri-sub-01","ninja-pri-sub-02"]
  
}
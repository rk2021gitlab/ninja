#VPC ID of Nework Module.
output "ninja_vpc_id" {
    value = aws_vpc.ninja_vpc.id
  
}

#IGW ID of the Network Module.
output "ninja_igw_id" {
    value = aws_internet_gateway.ninja_igw.id
  
}

#Public Subnet ID of the Network Module.
output "ninja_pub_subnet_id" {
    value = aws_subnet.public_subnet[*].id
  
}

#Private Subnet ID of the Network Module.
output "ninja_pri_subnet_id" {
    value = aws_subnet.private_subnet[*].id
  
}

#Private Subnet ID of the Network Module.
output "ninja_eip_id" {
    value = aws_eip.ninja_eip.id
  
}

#NAT Gateway ID
output "natgateway" {
    value = aws_nat_gateway.ninja_nat_gateway.id
  
}

#Public Route Table ID of the Network Module.
output "ninja_public_route_table_id" {
    value = aws_route_table.ninja_public_route.id
  
}

#Private Route Table ID of the Network Module.
output "ninja_private_route_table_id" {
    value = aws_route_table.ninja_private_route.id
  
}
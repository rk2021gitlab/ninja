#Bastian SG Id
output "Bastion-sg_id" {
    value = aws_security_group.Bastionsg.id
  
}

#Apache SG Id
output "Apache-sg_id" {
    value = aws_security_group.Apachesg.id
  
}

#Instance ID of Bastion Server.
output "Bastion_Server_id" {
    value = aws_instance.BastionServer.id
  
}

#Instance ID of Apache Server.
output "Apache_Server_id" {
    value = aws_instance.ApacheServer.id
  
}

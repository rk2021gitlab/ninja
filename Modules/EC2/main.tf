#Creating Security Group for Bastion Host.
resource "aws_security_group" "Bastionsg" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = var.ninja_vpc_id

  dynamic "ingress" {

    for_each = var.Bastion-ingress
    content {
      description = "TLS from VPC"
      from_port   = ingress.value.port
      to_port     = ingress.value.port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.port == 22 ? ["${chomp(data.http.myip.body)}/32"] : ingress.value.cidr_blocks

    }

  }
}

#launching Bastion Server.
resource "aws_instance" "BastionServer" {
  ami           = "ami-0e472ba40eb589f49"
  instance_type = "t2.micro"
  subnet_id = var.ninja_pub_subnet_id[0]
  vpc_security_group_ids = [aws_security_group.Bastionsg.id]
  key_name = "virginiakeypair"
  associate_public_ip_address = true

  tags = {
    Name = "BastionServer"
  }
}


#Creating Security Group for Apache Host.
resource "aws_security_group" "Apachesg" {
  name        = "Apache"
  description = "Allow SSH from Bastion"
  vpc_id      = var.ninja_vpc_id


  dynamic "ingress" {

    for_each = var.Apache-ingress
    content {
      description = "TLS from VPC"
      from_port   = ingress.value.port
      to_port     = ingress.value.port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.port == 22 ? ["${aws_instance.BastionServer.private_ip}/32"] : ingress.value.cidr_blocks

    }
  }
}

#launching Apache Server.
resource "aws_instance" "ApacheServer" {
  ami           = "ami-0e472ba40eb589f49"
  instance_type = "t2.micro"
  subnet_id = var.ninja_pub_subnet_id[0]
  vpc_security_group_ids = [aws_security_group.Apachesg.id]
  key_name = "virginiakeypair"

  tags = {
    Name = "ApacheServer"
  }
}

#security group variable.
variable "Bastion-ingress" {

  default = {
 
    "22" = {
      port        = 22
      protocol    = "tcp"

    }
  }
}

#VPC ID imported from Network Module.

variable "ninja_vpc_id" {
  
}

#Importing Public subnet Id from Network Module.
variable "ninja_pub_subnet_id" {
  
}

#Importing Private subnet Id from Network Module.
variable "ninja_pri_subnet_id" {
  
}


#security group variable.
variable "Apache-ingress" {

  default = {
    "443" = {
      port        = 443
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]

    }
    "80" = {
      port        = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]

    }
    "22" = {
      port        = 22
      protocol    = "tcp"

    }
  }
}

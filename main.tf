provider "aws" {
  region = var.region

}


module "Network" {
  source        = "./Modules/Network"
  vpccidr       = var.vpccidr
  pub-sub-names = var.pub-sub-names
  pri-sub-names = var.pri-sub-names
  
}

module "EC2" {
  source              = "./Modules/EC2"
  ninja_vpc_id          = module.Network.ninja_vpc_id
  ninja_pub_subnet_id   = module.Network.ninja_pub_subnet_id
  ninja_pri_subnet_id   = module.Network.ninja_pri_subnet_id

}

#Output of VPC ID 
output "vpc_id" {
  value = module.Network.ninja_vpc_id

}

#Output of Public Subnets
output "public-subnet-id" {
  value = module.Network.ninja_pub_subnet_id

}

#Output of Private Subnets
output "private-subnet-id" {
  value = module.Network.ninja_pri_subnet_id

}

output "pub-sub-names" {
  value = var.pub-sub-names
}

output "pri-sub-names" {
  value = var.pri-sub-names
}

output "vpccidr" {
  value = var.vpccidr
}

